using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MoveAim : MonoBehaviour ,IDragHandler,IPointerUpHandler,IPointerDownHandler
{
    [SerializeField] private Shoot shoot;
    private Image bg;
    [SerializeField] private Image joystik;
    private Vector2 inputVector;

    private void Start()
    {
        bg = GetComponent<Image>();
        joystik = transform.GetChild(0).GetComponent<Image>();
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (!BlockLogic.canShoot) return;
        Vector2 pos;
        if(RectTransformUtility.ScreenPointToLocalPointInRectangle(bg.rectTransform,eventData.position,eventData.pressEventCamera,out pos))
        {
            pos.x = (pos.x / bg.rectTransform.sizeDelta.x);
            pos.y = (pos.y / bg.rectTransform.sizeDelta.y);

            inputVector = new Vector2(pos.x * 2 - 1, pos.y * 2 - 1);
            inputVector = (inputVector.magnitude > 1) ? inputVector.normalized : inputVector;

            joystik.rectTransform.anchoredPosition = new Vector2(inputVector.x * (bg.rectTransform.sizeDelta.x / 2), inputVector.y * (bg.rectTransform.sizeDelta.y / 2));
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (!BlockLogic.canShoot) return;
        OnDrag(eventData);
        joystik.rectTransform.anchoredPosition = Vector2.zero;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (!BlockLogic.canShoot) return;
        shoot.SpawnBlocks();
        inputVector = default;
        joystik.rectTransform.anchoredPosition = Vector2.zero;
    }

   public float Horizontal()
    {
        if (inputVector.x != 0) return inputVector.y;
        else return Input.GetAxis("Horizontal");
    }

    public float Vertical()
    {
        if (inputVector.y != 0) return inputVector.x;
        else return Input.GetAxis("Vertical");
    }
}
