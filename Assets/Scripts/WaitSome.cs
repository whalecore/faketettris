﻿using System.Collections;
using UnityEngine;


public class WaitSome : MonoBehaviour
{
    public float time = 3f;

    private void Start()
    {
        StartCoroutine(Wait(time));
    }

    IEnumerator Wait(float time = 3)
    {
        yield return new WaitForSeconds(time);
        Destroy(this.gameObject);
    }
}
