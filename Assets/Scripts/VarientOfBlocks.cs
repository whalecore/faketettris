﻿using System.Collections;
using UnityEngine;

[System.Serializable]
public class VarientOfBlocks : IVarientOfBlock
{
    [SerializeField] private GameObject blocks;

    public GameObject ReturnVarientOfBlock()
    {
        blocks = GameObject.Find("Cube");
        return blocks;
    }
}


interface IVarientOfBlock
{
    GameObject ReturnVarientOfBlock();
}