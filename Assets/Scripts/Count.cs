﻿using System.Collections;
using UnityEngine;

public class Count : MonoBehaviour
{
    private float catetOne, gipotenyzaX, gipotenyzaY, catetTwo;
    private const float time = 1f,timeAxis = 0.9f;
    public BaseLogicForPlayers baseLogic;

    private void Start()
    {
        if (gameObject.CompareTag("EnemyCastle"))
            baseLogic = new EnemyBot(transform);
        if (gameObject.CompareTag("PlayerCastle"))
            baseLogic = new PlayerLogic(transform);
        baseLogic.catetOne = (baseLogic.EndPos() - baseLogic.transformThis.position).magnitude;
    } 

    private void Update()
    {
        baseLogic.gipX = (new Vector3(baseLogic.EndPos().x, 0, baseLogic.EndPos().z) - new Vector3(baseLogic.transformThis.position.x, 0, baseLogic.transformThis.position.z)).magnitude;
        baseLogic.gipY = (new Vector3(0, baseLogic.EndPos().y, baseLogic.EndPos().z) - new Vector3(0, baseLogic.transformThis.position.y, baseLogic.transformThis.position.z)).magnitude;
        baseLogic.speedZ = baseLogic.gipX / time;
        baseLogic.speedX = CountDistance(baseLogic.gipX, baseLogic.catetOne) / timeAxis;
        baseLogic.speedY = CountDistance(baseLogic.gipY, baseLogic.catetOne) / timeAxis;
    }

    private static float CountDistance(float gip,float firstPoint) => Mathf.Sqrt(Mathf.Pow(gip, 2) - Mathf.Pow(firstPoint, 2));
}

public abstract class BaseLogicForPlayers
{
    public abstract Vector3 EndPos();
    public Transform target,transformThis;
    public Color color,onWall;
    public string tag;
    public float speedZ, speedX, speedY , catetOne,gipX,gipY;
}

public class EnemyBot : BaseLogicForPlayers
{
    //new public float speedZ, speedX, speedY;
    private Transform transform ;

    public EnemyBot(Transform transform)
    {
        base.gipX = gipX;
        base.gipY = gipY;
        base.transformThis = transform;
        base.tag = "Enemy";
        base.color = Color.blue;
        base.onWall = Color.red;
        this.transform = transform;
        base.speedZ = speedZ;
        base.speedX = speedX;
        base.speedY = speedY;
        base.target = BotAi.TargetPos(transform);
        base.catetOne = catetOne;
    }

    public override Vector3 EndPos()
    {
        Ray ray = new Ray(new Vector3(transform.position.x, transform.position.y, transform.position.z + .5f), transform.forward);
        Physics.Raycast(ray, out RaycastHit hit, 20);
        Debug.DrawLine(transform.position, hit.point, Color.red);
        return hit.point;
    }
}

public class PlayerLogic : BaseLogicForPlayers
{
    private Transform transform;
    //new public float speedZ, speedX, speedY;

    public PlayerLogic(Transform transform)
    {
        base.gipX = gipX;
        base.gipY = gipY;
        base.transformThis = transform;
        base.tag = "Player";
        base.color = Color.red;
        base.onWall = Color.blue;
        this.transform = transform;
        base.speedZ = speedZ;
        base.speedX = speedX;
        base.speedY = speedY;
        base.catetOne = catetOne;
        base.target = GameObject.FindGameObjectWithTag("Aim").transform;
    }

    public override Vector3 EndPos()
    {
        Ray ray = new Ray(new Vector3(transform.position.x, transform.position.y, transform.position.z + .5f), transform.forward);
        Physics.Raycast(ray, out RaycastHit hit, 20);
        return hit.point;
    }
}
