﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ControlAim : MonoBehaviour
{
    public GameObject aim;
    [SerializeField] private Material material;
    public float speed;
    public MoveAim move;

    private void Update()
    {
        MakeRay();
        MoveRay();
    }

    public void ChangeAim()
    {
        Destroy(aim.transform.GetChild(0).gameObject);
        var obj = Instantiate(Next_Blocks.CurrentFigure(), aim.transform.position, Quaternion.identity, aim.transform);
        Destroy(obj.GetComponent<BoxCollider>());
        Destroy(obj.GetComponent<BlockLogic>());
        Destroy(obj.GetComponent<Rigidbody>());
        MeshRenderer[] materials = obj.transform.GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer mesh in materials)
        {
            mesh.material = material;
            mesh.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            mesh.receiveShadows = false;
        }
    }

    private void MoveRay()
    {
        transform.Rotate(move.Horizontal() * -speed * Time.deltaTime, move.Vertical() * speed * Time.deltaTime, 0, Space.Self);
    }

    private void MakeRay()
    {
        Ray ray = new Ray(new Vector3(transform.position.x, transform.position.y, transform.position.z + .5f), transform.forward);
        if (Physics.Raycast(ray, out RaycastHit hit, 20))
        {
            if (aim.transform.position != hit.point)
                aim.transform.position = hit.point;
        }
    }

}
