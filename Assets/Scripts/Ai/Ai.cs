using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Ai : MonoBehaviour
{
    public BaseAi baseAi;
    private NavMeshAgent navMeshAgent;
    private bool isLocked;
    public Collider[] colliders;

    private void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();

        if (gameObject.CompareTag("Enemy"))
            baseAi = new EnemyAi();
        if (gameObject.CompareTag("Player"))
            baseAi = new PlayerAi();
    }

    private void Update()
    {
        navMeshAgent.SetDestination(PosToFollow());
    }

    private Vector3 PosToFollow()
    {
        Collider[] collider = Physics.OverlapSphere(transform.position, 20);
        colliders = collider;
        for (int i = 0; i < collider.Length; i++)
        {
            if (collider[i].CompareTag(baseAi.name) && !isLocked)
            {
                isLocked = true;
                return collider[i].transform.position;
            }

            if (!collider[i].CompareTag(baseAi.name)) 
            {
                isLocked = false;
                return baseAi.TowerPosition().position; 
            }
        }
        return baseAi.TowerPosition().position;
    }
}
