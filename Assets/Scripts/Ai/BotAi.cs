﻿using System.Collections;
using UnityEngine;


public class BotAi : MonoBehaviour
{
    public GameObject obj;
    private GameObject[] wallBlocks;
    [SerializeField] private GameObject[] blocks;
    private GameObject lookAt;
    public static Transform target;
    public GameObject previousPos;
    private int _previousIndex;
    private float time = 2f;

    private void Awake()
    {
        wallBlocks = GameObject.FindGameObjectsWithTag("Wall");
        lookAt = wallBlocks[Random.Range(0, wallBlocks.Length)];
    }

    private void Update()
    {
            time -= Time.deltaTime;
            if (time <= 0)
                Shoot();

        transform.LookAt(lookAt.transform);
        //target = TargetPos(transform);
    }

    private void Shoot()
    {
        Instantiate(Spawn(), transform.position, Quaternion.identity);

        lookAt = wallBlocks[Random.Range(0, wallBlocks.Length)];
        while (lookAt.GetComponent<MeshRenderer>().material.color == Color.blue)
        {
            lookAt = wallBlocks[Random.Range(0, wallBlocks.Length)];
        }
        time =  Random.Range(2, 4);
    }

    public static Transform TargetPos(Transform transform)
    {
        Ray ray = new Ray(new Vector3(transform.position.x, transform.position.y, transform.position.z + .5f), transform.forward);
        Physics.Raycast(ray, out RaycastHit hit, 20);
        return hit.transform;
    }


    private GameObject Spawn()
    {
        _previousIndex = 0;
        var index = Random.Range(0, blocks.Length);
        while (_previousIndex == index)
        {
            index = Random.Range(0, blocks.Length);
        }
        previousPos = blocks[index];
        _previousIndex = index;
        return blocks[index];
    }
}
