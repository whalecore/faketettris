﻿using UnityEngine;

public abstract class BaseAi
{
    public string name;
    public Transform defaultPos;

    public abstract Transform TowerPosition();
}

public class EnemyAi : BaseAi
{
    public EnemyAi()
    {
        base.name = "Player";
    }

    public override Transform TowerPosition()
    {
        return base.defaultPos = GameObject.FindGameObjectWithTag("PlayerCastle").transform;
    }
}

public class PlayerAi : BaseAi
{
    public PlayerAi()
    {
        base.name = "Enemy";
    }

    public override Transform TowerPosition()
    {
        return base.defaultPos = GameObject.FindGameObjectWithTag("EnemyCastle").transform;
    }
}
