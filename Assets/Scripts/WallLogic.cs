﻿using System;
using System.Collections;
using UnityEngine;
using System.Linq;
using System.Threading.Tasks;

public class WallLogic : MonoBehaviour
{
    private static GameObject[,] walls = new GameObject[4, 8];
    private GameObject[] wallBlocks;
    public GameObject cube;
    private int k = 0;
    private BaseLogicForPlayers baseLogic;

    private void Awake()
    {
        wallBlocks = GameObject.FindGameObjectsWithTag("Wall");
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                walls[i, j] = wallBlocks[k];
                k++;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("EnemyBlock"))
            baseLogic = GameObject.FindGameObjectWithTag("EnemyCastle").GetComponent<Count>().baseLogic;
        if (other.CompareTag("PlayerBlock"))
            baseLogic = GameObject.FindGameObjectWithTag("PlayerCastle").GetComponent<Count>().baseLogic;
        StartCoroutine(Wait());
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(.1f);
        CheckArrayForFullColums();
        CheckArrayForFullRows();

    }

    private void CheckArrayForFullRows()
    {
        GameObject[] arr = new GameObject[8];

        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 8; j++)
                arr[j] = walls[i, j];
            NewColor(arr, baseLogic.color);
            Array.Clear(arr, 0, 4);
        }
    }

    private void CheckArrayForFullColums()
    {
        GameObject[] arr = new GameObject[4];

        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 4; j++)
                arr[j] = walls[j, i];
            NewColor(arr, baseLogic.color);
            Array.Clear(arr, 0, 4);
        }
    }

    private void NewColor(GameObject[] arr, Color def)
    {
        if (arr.All(arr => arr.GetComponent<MeshRenderer>().material.color == def))
        {
            foreach (GameObject obj in arr)
            {
                obj.GetComponent<MeshRenderer>().material.color = Color.white;
                SpawnNewMinions(obj.transform, baseLogic.tag);
            }
        }
    }

    private void SpawnNewMinions(Transform transform, string tag)
    {
        var obj = Instantiate(cube, transform.position - new Vector3(0, 0, 1), Quaternion.identity);
        obj.transform.localScale = new Vector3(.2f, .2f, .2f);
        obj.GetComponent<MeshRenderer>().material.color = Color.green;
        obj.name = "Im new";
        obj.tag = tag;
    }
}
