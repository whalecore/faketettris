using UnityEngine;

public class Shoot : MonoBehaviour
{
    [SerializeField] private GameObject allBlocks; // obj в испекторе
    [SerializeField] private Transform startPos;
    [SerializeField] private GameObject[] blocks;
    public int currentBlock;

    public void SpawnBlocks()
    {
        BlockLogic.canShoot = false;
        Instantiate(blocks[currentBlock], startPos.position, Quaternion.identity);
        allBlocks.GetComponent<Next_Blocks>().NewBlock();
    }

   
}