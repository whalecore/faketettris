using UnityEngine;

// Новые блоки
public class Next_Blocks : MonoBehaviour
{
    [SerializeField] private GameObject GunPlayer; // obj в испекторе
    [SerializeField] public GameObject[] _allBlocks;
    [SerializeField] private ControlAim controlAim;
    public int lastFigure;
    private static GameObject currentObj;

    private void Start()
    {
        int randomShapes = Random.Range(0, _allBlocks.Length);
        currentObj = _allBlocks[randomShapes];
        currentObj.SetActive(true);
        controlAim.ChangeAim();
        _allBlocks[randomShapes].SetActive(true);
        lastFigure = randomShapes;
        GunPlayer.GetComponent<Shoot>().currentBlock = randomShapes;
    }

    public void NewBlock()
    {
        _allBlocks[lastFigure].SetActive(false);
        int randomShapes = Random.Range(0, _allBlocks.Length);
        _allBlocks[randomShapes].SetActive(true);
        currentObj = _allBlocks[randomShapes];
        currentObj.SetActive(true);
        lastFigure = randomShapes;
        GunPlayer.GetComponent<Shoot>().currentBlock = lastFigure;
    }

    public static GameObject CurrentFigure()
    {
        return currentObj;
    }
}