using UnityEngine;

// Сетка
public class BlockLogic : MonoBehaviour
{
    [SerializeField] private ControlAim controlAim = null;
    public static bool canShoot = true;
    private BaseLogicForPlayers baseLogic;

    private void Awake()
    {
        if (transform.CompareTag("EnemyBlock"))
            baseLogic = GameObject.FindGameObjectWithTag("EnemyCastle").GetComponent<Count>().baseLogic;
        if (transform.CompareTag("PlayerBlock"))
            baseLogic = GameObject.FindGameObjectWithTag("PlayerCastle").GetComponent<Count>().baseLogic;
        controlAim ??= GameObject.FindGameObjectWithTag("PlayerCastle").GetComponent<ControlAim>();
    }

    private void Update()
    {
        MoveObj();
        transform.Translate(Vector3.forward * baseLogic.speedZ * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!canShoot)
            controlAim.ChangeAim();

        canShoot = true;

        if (other.CompareTag("Collider"))
        {
            return;
        }

        if (!other.CompareTag("Platform"))
        {
            EnterOnTrigger(other, baseLogic.color);
        }

        if (other.CompareTag("Trigger"))
        {
            EnterOnTrigger(other, default);
        }
    }

    private void EnterOnTrigger(Collider other, Color color)
    {
        baseLogic.speedZ = 0;

        if (other.GetComponent<MeshRenderer>().materials[0].color == color)
        {
            return;
        }
        else if (other.GetComponent<MeshRenderer>().materials[0].color == baseLogic.onWall)
        {
            DestroyBlock(other, Color.white);
        }
        else
        {
            DestroyBlock(other, color);
        }
        Destroy(this.gameObject);

        void DestroyBlock(Collider other, Color color)
        {
            other.gameObject.GetComponent<MeshRenderer>().materials[0].color = color;
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).gameObject.AddComponent<Rigidbody>().AddExplosionForce(200, this.transform.position, 30);
                //transform.GetChild(i).gameObject.AddComponent<BoxCollider>();
                transform.GetChild(i).gameObject.AddComponent<WaitSome>();
                transform.GetChild(i).transform.SetParent(null);
            }
        }
    }

    private void MoveObj()
    {
        if (baseLogic.target.position.y > baseLogic.transformThis.position.y)
        {
            transform.Translate(Vector3.up * baseLogic.speedY * Time.deltaTime);
        }

        if (baseLogic.target.position.y < baseLogic.transformThis.position.y)
        {
            transform.Translate(Vector3.down * baseLogic.speedY * Time.deltaTime);
        }

        if (baseLogic.target.position.x > baseLogic.transformThis.position.x)
        {
            transform.Translate(Vector3.right * baseLogic.speedX * Time.deltaTime);
        }

        if (baseLogic.target.position.x < baseLogic.transformThis.position.x)
        {
            transform.Translate(Vector3.left * baseLogic.speedX * Time.deltaTime);
        }
    }
}